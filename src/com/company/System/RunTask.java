package com.company.System;

import com.company.Product.Catalog;
import com.company.Product.Product;
import com.company.User.Account;
import com.company.User.Address;
import com.company.User.Admin;
import com.company.User.Customer;
import com.company.User.Guest;
import com.company.User.Member;
import com.company.categories.ProductCategory;
import com.company.core.ShopManager;
import com.company.Product.Item;
import com.company.Product.Order;
import com.company.Bank.CreditCardTransaction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class RunTask {
    // here we will write menu (window of accounts)

    static Member currentMember;
    static Admin currentAdmin;
    static Guest currentGuest;
    static Customer currentCustomer;
    
    ArrayList<Item> currentShoppingCart = new ArrayList<>();

	//ShopManager sm = new ShopManager();

    Scanner sc = new Scanner(System.in);

    public RunTask (){
        currentMember = null;
        currentAdmin = null;
        currentGuest = null;
    }

    int choice = -1;
    Scanner scan = new Scanner(System.in);

    public boolean Authorization(){
        if (currentMember!= null){
            return true;
        }
        while(true){
            System.out.println("Hey! type actions to switch another page...");
            System.out.println("*-----------------*|1.Sign in|*-----------------*"+
                             "\n*-----------------*|2.Sign up|*-----------------*" +
                             "\n*-----------------*|0. Quit  |*-----------------*");

            choice = scan.nextInt();
            if(choice == 0){
                return false;
            }
            if(choice == 1){
                SignIn();
            }
            else if(choice == 2){
                currentMember = new Member ();
            }
            else{
                System.out.println("Type corrects symbols");
            }
            if(currentMember == null){
            Authorization();
            }
            else return true;
        }

    }
    public Member SignUp() {

        System.out.println("1) Login:");
        String login = sc.next();
        System.out.println("2) Password:");
        String password = sc.nextLine();
        System.out.println("3) Type your name: ");
        String name = sc.nextLine();
        System.out.println("4) Type your address");
        //Address address = UserAddress();
        System.out.println("5) Type your e-mail: ");
        String email = sc.nextLine();
        System.out.println("6) Type your phone: ");
        String phone = sc.nextLine();

        //currentMember = new Member(new Account(login, password, name, address, email, phone));

        return currentMember;
    }
	public void Start() {

		while (true) {
			System.out.println("*---------*|Amazon shop 2.0 edition|*---------*");
            /// categories
            System.out.println("Type #-1. to Continue or #-2. to authorization");
            String choosing = sc.nextLine();
            if(choosing.equals("1")){
                    CategoryPart();
                    
            }
            else if(choosing.equals("2")){
                Authorization();
            }
        }
    }
    
    public void CategoryPart(){

        Catalog catalog = new Catalog ();
        
        while(true){
        
            System.out.println("Categories:");
            System.out.println("type 0 to exit");
        
            int cnt = 0;
        
            for (ProductCategory i : ShopManager.getAllCategories()){
                System.out.println(cnt + ") " + i.getName());
                cnt++;
            }
            
            System.out.println("Type the name of one category: ");
            String chooseCategory = (sc.nextLine()).toLowerCase();
            
            if(chooseCategory == "0"){
                System.out.println("~~See you next time!~~");
            break;
            }

            if(catalog.SearchProductsByCategory(chooseCategory) == null){
                System.out.println("Type correct symbols");
                break;
            }
            else if (catalog.SearchProductsByCategory(chooseCategory) != null){
                showProductsByCategory(catalog.SearchProductsByCategory(chooseCategory));
            }
            

            // System.out.println("If you want to order something then type 'yes' or press 0 to finish...");
            // String action = sc.nextLine().toLowerCase();

            // if(action.equals("yes")){
            //     //Action();
            // }

            // else if(action.equals("0")){
            //     break;
            // }

            // else{
            //        System.out.println("Type correct symbols"); 
            // }
        }
    }

    public void showProductsByCategory (ArrayList<Product> products){
        int cnt = 1;
        for (Product i : products){
            System.out.println(cnt + ") " + i.getName());
            cnt++;
        }
        System.out.println("Choose products with numbers");
        Scanner sc = new Scanner (System.in);
        int h = sc.nextInt();
        cnt = 1;
        for (Product i : products){
            if(cnt == h){
                showSelectedProduct (i);
            }
            cnt++;
        }
        sc.close();
    }

    public void showSelectedProduct (Product product){
        Scanner sc = new Scanner (System.in);
        System.out.println(product.toString());
        System.out.println("Write <yes> to buy, otherwise write anything");
        String s = sc.next();
        if (s.toLowerCase() == "yes"){
            currentCustomer.addItemToCart(new Item(product));
            System.out.println("Do you want to continue shopping? Type yes or otherwise will make payment.");
            s = sc.next().toLowerCase();
            sc.close ();
            if (s == "yes"){
                return ;
                //CategoryPart();
            }
            else {
                if (currentCustomer instanceof Member){
                    Member member = (Member) currentCustomer;
                    for (Member i : ShopManager.getAllMembers()){
                        if(i.getAccount().getId() == member.getAccount().getId()){
                            i.addOrder(new Order(member.getShoppingCart(),i.getAccount().getId()));
                            return ;
                        }
                    }
                }
               
                    System.out.println("Please create account or type correct symbols");
                    Authorization();
            }
        }
    }
    public Member SignIn(){

        System.out.println("1) Login:");
		String login = sc.nextLine();
		System.out.println("2) Password:"); 
        String password = sc.nextLine();
		for (Member i : ShopManager.getAllMembers()) {
            if(i.getAccount().getUserPassword() == password && i.getAccount().getUserName() == login){
                currentMember = i;
              break;
            }
        }
       return currentMember;
    }

    public boolean CheckForAuthorization(){ return false; }

}
