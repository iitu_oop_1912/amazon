package com.company.System;

import com.company.User.Admin;

import com.company.User.Member;
import com.company.categories.ProductCategory;
import com.company.core.ShopManager;
import com.company.Product.Product;
import java.util.Scanner;
public class RunAdmin {
    
    public void AdminMenu(){
        while(true){
        Scanner sc = new Scanner (System.in);
        System.out.println ("~~Admin Panel~~");
        System.out.println ("Select actions" + 
        "\n1. Add user" +  
        "\n2. Modify product category" +
        "\n3. Modify user status" + 
        "\n0. Quit");

        String choice = sc.nextLine();
        if(choice == "0"){
            return;
        }
        if(choice == "1"){
            RunTask.currentAdmin.addNewUser();
            //admin.
        }
        if(choice == "2"){
            int cnt = 1;
            System.out.println ("Choose Product to modify:");
            for (Product i : ShopManager.getAllProducts()){
                System.out.println (cnt + ") " + i.toString());
                cnt ++;
            }
            int h = sc.nextInt();
            for (Product i : ShopManager.getAllProducts()){
                if ( h ==cnt) {
                    i.ModifyProduct();
                }
                cnt ++;
            }
        }
        if(choice == "3"){
            int cnt = 1;
            System.out.println ("Choose Member to modify:");
            for (Member i : ShopManager.getAllMembers()){
                System.out.println (cnt + ") " + i.toString());
                cnt ++;
            }
            int h = sc.nextInt();
            
            for(Member i : ShopManager.getAllMembers()){
                if(i.getAccount().getId() == h){
                    System.out.println ("~~Choose status~~");
                    System.out.println ("Archive");
                    System.out.println ("Ban");
                    System.out.println ("Block");
                    String str = sc.nextLine().toLowerCase();
                    if(str.equals("archive")){
                        RunTask.currentAdmin.ArchiveUser(h);
                    }
                    else if(str.equals("ban")){
                        RunTask.currentAdmin.BannedUser(h);
                    }
                    else if(str.equals("block")){
                        RunTask.currentAdmin.blockUser(h);
                    }
                    else{
                        System.out.println ("Type correct symbols");
                    }
                }
            }
            }
        }   
    }
}
