package com.company.System;

import com.company.Product.Item;

import java.util.*;

public class ShoppingCart {
    public List<Item> items;

    // public void addItem (Item item){
    //     items.add (item);
    // }

    public ShoppingCart(){
        items = new ArrayList<>();
    }
    public ShoppingCart(List<Item> items) {
        this.items = items;
    }

    public int CalculatePrice (){
        int cnt = 0;
        for (Item i : items){
            cnt += i.getPrice();
        }
        return cnt;
    }

    public List<Item> getItems() {
        return items;
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }

    public boolean addItem(Item i){
    try{
        items.add(i);
        return true;
    } catch (Exception e) {
        return false;
    }
    }
    public boolean RemoveItem(int i){
        try{
            int j = 1;
            for (Item it: items) {
                if (j == i) {
                    items.remove(j);
                    return true;
                }
                j++;
            }
        }
        catch (Exception e) {
            return false;
        }
        return false;
    }

    public List<Item> GetItems(){
     return this.items;
    }

}
