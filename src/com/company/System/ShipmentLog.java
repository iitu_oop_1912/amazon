package com.company.System;

import com.company.Enums.ShipmentStatus;

import java.util.Date;

public class ShipmentLog {
    private ShipmentStatus status;
    private Date creationDate = new Date();

    public ShipmentLog(ShipmentStatus status, Date creationDate) {
        this.status = status;
        this.creationDate = creationDate;
    }

    public ShipmentLog() {
    }

    public ShipmentStatus getStatus() {
        return status;
    }

    public void setStatus(ShipmentStatus status) {
        this.status = status;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    @Override
    public String toString() {
        return "ShipmentLog{" +
                "status=" + status +
                ", creationDate=" + creationDate +
                '}';
    }
}
