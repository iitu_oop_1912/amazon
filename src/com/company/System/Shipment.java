package com.company.System;

//import java.sql.Date;
import java.util.ArrayList;
        import java.util.Date;

public class Shipment {
    private Date shipmentDate;
    private Date estimatedArrival;
    private String  shipmentMethod;  private ArrayList <ShipmentLog> logs;
  
    public Shipment(Date estimatedArrival, String shipmentMethod) {
        this.shipmentDate = new Date ();
        this.estimatedArrival = estimatedArrival;
        this.shipmentMethod = shipmentMethod;
        this.logs = new ArrayList<>();
    }

    public boolean addShipmentLog (ShipmentLog shipmentLog){
        try {
            logs.add (shipmentLog);
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    } 
}
