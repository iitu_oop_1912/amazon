package com.company.Bank;

import com.company.Enums.PaymentStatus;
import com.company.User.Account;
import com.company.User.Member;
import com.company.core.ShopManager;
import java.util.Scanner;
public class CreditCardTransaction extends Payment {

    public CreditCardTransaction() {

    }
    public PaymentStatus processPayment (int ownerId){
        Scanner sc = new Scanner(System.in);
        for (Member i : ShopManager.getAllMembers()){
            if (ownerId ==  i.getAccount().getId()){
                if (i.getAccount().getShippingAddress() != null){
                    System.out.println("Choose options below : "+
                    "\n1) Pay" + 
                    "\n2) Quit");
                    String choice = sc.nextLine();
                    if(choice.equals("1")){
                        System.out.println("Transaction completed successfully");
                        return PaymentStatus.Completed;
                    }
                    else if(choice.equals("2")){
                        System.out.println("Transaction canceled");
                        return PaymentStatus.Canceled;
                    }
                }
            }
            else{
                System.out.println("~~Transaction declined~~");
                return PaymentStatus.Declined;
            }
        }
        return status;
    }    
        
}
