package com.company.Bank;

public class ElectronicBank {
    private String bankName;
    private String rountingName;
    private String accountNumber;

    public ElectronicBank(String bankName, String rountingName, String accountNumber) {
        this.bankName = bankName;
        this.rountingName = rountingName;
        this.accountNumber = accountNumber;
    }

    public String getBankName() {
        return this.bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getRountingName() {
        return this.rountingName;
    }

    public void setRountingName(String rountingName) {
        this.rountingName = rountingName;
    }

    public String getAccountNumber() {
        return this.accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

}
