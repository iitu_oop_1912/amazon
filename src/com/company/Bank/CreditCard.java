package com.company.Bank;

import com.company.Bank.Payment;
import com.company.User.Address;

public class CreditCard {
    private String nameofCard;
    private String cardNumber;
    private int code;
    private Address billingAdress;

    public CreditCard(){

    }
    public CreditCard(String nameofCard, String cardNumber, int code, Address billingAdress) {
        this.nameofCard = nameofCard;
        this.cardNumber = cardNumber;
        this.code = code;
        this.billingAdress = billingAdress;
    }

    public String getNameofCard() {
        return this.nameofCard;
    }

    public void setNameofCard(String nameofCard) {
        this.nameofCard = nameofCard;
    }

    public String getCardNumber() {
        return this.cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public int getCode() {
        return this.code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public Address getBillingAdress() {
        return this.billingAdress;
    }

    public void setBillingAdress(Address billingAdress) {
        this.billingAdress = billingAdress;
    }

}
