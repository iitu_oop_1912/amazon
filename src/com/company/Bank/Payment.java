package com.company.Bank;

import com.company.Enums.PaymentStatus;
import com.company.Product.Order;
import com.company.User.Account;
import com.company.User.Member;

public abstract class Payment{

    protected PaymentStatus status;
    protected double amount;

    public Payment(PaymentStatus status, double amount) {
        this.status = status;
        this.amount = amount;
    }
        
    abstract public PaymentStatus processPayment(int ownerId);

    public Payment() {
    }

    public PaymentStatus getStatus() {
        return status;
    }

    public void setStatus(PaymentStatus status) {
        this.status = status;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    @Override
    public String toString() {
        return "Payment{" +
                "status=" + status +
                ", amount=" + amount +
                '}';
    }

}
