package com.company.User;

import java.util.ArrayList;
import java.util.Scanner;

import com.company.Bank.CreditCard;
import com.company.Bank.ElectronicBank;
import com.company.Product.Order;
import com.company.System.Notification;
import com.company.core.ShopManager;

public class Member extends Customer{
    private Account account;
    
    private CreditCard creditCard;
    private ElectronicBank electronicBank;
    private ArrayList <Order> listOfOrders;
    private ArrayList <Notification> listOfNotifications;

    public Member(Account account) {
        this.account = account;
        this.creditCard = null;
        this.electronicBank = null;
        this.listOfOrders = null;
    }

    public Member(){
        Scanner sc = new Scanner (System.in);
        System.out.println("1) Login:");
        String login = sc.next();
        sc.nextLine ();
		System.out.println("2) Password:");
		String password = sc.nextLine();
		System.out.println("3) Type your name: ");
		String name = sc.nextLine();
		System.out.println("4) Type your address");
		Address address = new Address();
		System.out.println("5) Type your e-mail: ");
		String email = sc.nextLine();
		System.out.println("6) Type your phone: ");
		String phone = sc.nextLine();
        
        this.creditCard = new CreditCard();

        this.account = new Account(login, password, name, address, email, phone);
        ShopManager.addMember (this);

    }

    public void addNotification (Notification notification){
        listOfNotifications.add (notification);
    }

    public void showNotifications (){
        Scanner sc = new Scanner (System.in);
        System.out.println ("There is List of Notifications :");
        System.out.println ("Press 1 to delete all Notifications");
        System.out.println ("Press 2 to exit");
        for (Notification i : listOfNotifications){
            System.out.println ("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
            System.out.println (i.toString());
        }
        int h = sc.nextInt();
        if (h == 1) {
            listOfNotifications.removeAll(listOfNotifications);
        }
        sc.close ();
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public CreditCard getCreditCard() {
        return this.creditCard;
    }

    public void setCreditCard(CreditCard creditCard) {
        this.creditCard = creditCard;
    }

    public ElectronicBank getElectronicBank() {
        return this.electronicBank;
    }

    public void setElectronicBank(ElectronicBank electronicBank) {
        this.electronicBank = electronicBank;
    }

    public ArrayList<Order> getListOfOrders() {
        return this.listOfOrders;
    }

    public void setListOfOrders(ArrayList<Order> listOfOrders) {
        this.listOfOrders = listOfOrders;
    }
    public void addOrder (Order order){
        this.listOfOrders.add (order);
    }


    public ArrayList<Notification> getListOfNotifications() {
        return this.listOfNotifications;
    }

    public void setListOfNotifications(ArrayList<Notification> listOfNotifications) {
        this.listOfNotifications = listOfNotifications;
    }

}
