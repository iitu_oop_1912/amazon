package com.company.User;

import com.company.Enums.AccountStatus;
import com.company.Product.Product;
import com.company.categories.ProductCategory;
import com.company.core.ShopManager;

public class Admin{
    private Account account;

    public boolean blockUser (int user_id){
        for (Member i : ShopManager.getAllMembers()){
            if (i.getAccount().getId() == user_id){
                i.getAccount().setUserStatus(AccountStatus.Blocked);
                return true;
            }
        }
        return false;
    }
    public boolean ArchiveUser(int user_id){
        for (Member i : ShopManager.getAllMembers()){
            if (i.getAccount().getId() == user_id){
                i.getAccount().setUserStatus(AccountStatus.Archived);
                return true;
            }
        }
        return false;
    }
    public boolean BannedUser (int user_id){
        for (Member i : ShopManager.getAllMembers()){
            if (i.getAccount().getId() == user_id){
                i.getAccount().setUserStatus(AccountStatus.Banned);
                return true;
            }
        }
        return false;
    }


    public boolean addNewUser(){
        Member mem = new Member();
        ShopManager.addMember(mem);
        return false;
    }

}
