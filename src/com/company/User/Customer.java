package com.company.User;

import com.company.Product.Item;
import com.company.System.ShoppingCart;

public abstract class Customer {
    protected ShoppingCart shoppingCart;
    public ShoppingCart getShoppingCart (){
        return shoppingCart;
    }
	public void setShoppingCart(ShoppingCart shoppingCart) {
        this.shoppingCart = shoppingCart;
    }
    public Customer (){
        shoppingCart = new ShoppingCart();  
    }
    public void addItemToCart (Item item){
        this.shoppingCart.addItem (item);
    }
}
