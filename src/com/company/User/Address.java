package com.company.User;

import java.util.Scanner;

public class Address {
    private String streetAddress;
    private String city;
    private String state;
    private String zipcode;
    private String country;

    public Address(String streetAddress, String city, String state, String zipcode, String country) {
        this.streetAddress = streetAddress;
        this.city = city;
        this.state = state;
        this.zipcode = zipcode;
        this.country = country;
    }
    public Address (){
        Scanner sc = new Scanner (System.in);
        System.out.println("1) Type the Street address");
		String streetAddress = sc.nextLine();
		System.out.println("2) Type your city");
        String city = sc.nextLine();
        System.out.println("3) Type the State");
		String state = sc.nextLine();
		System.out.println("4) Type your zipcode");
		String zipcode = sc.nextLine();
        System.out.println("5) Type your country");
		String country = sc.nextLine();
        
        this.streetAddress = streetAddress;
        this.city = city;
        this.state = state;
        this.zipcode = zipcode;
        this.country = country;
    }

    public String getStreetAddress() {
        return streetAddress;
    }

    public void setStreetAddress(String streetAddress) {
        this.streetAddress = streetAddress;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
