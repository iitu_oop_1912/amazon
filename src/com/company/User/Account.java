package com.company.User;

import com.company.Bank.CreditCard;
import com.company.Bank.ElectronicBank;
import com.company.Enums.AccountStatus;
import com.company.System.ShoppingCart;

public class Account {
    private static  int last_id = 0;
    private int Id;
    protected String userName;
    protected String userPassword;
    protected AccountStatus userStatus;
    protected String name;
    protected Address shippingAddress;
    protected String email;
    protected String phoneNumber;   

    public Account(String userName, String userPassword, String name, Address shippingAddress, String email, String phoneNumber) {
        this.userName = userName;
        this.userPassword = userPassword;
        this.name = name;
        this.shippingAddress = shippingAddress;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.Id = last_id;
        this.userStatus = AccountStatus.Active;
        last_id++;
        
    }

    public Address getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(Address shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public AccountStatus getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(AccountStatus userStatus) {
        this.userStatus = userStatus;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    //abstract Address GetShippingAddress();
    public boolean AddProductReview(){
        return false;
    }
    public boolean AddProduct(){
        return false;
    }


    public String toString(){
        return "Guest{" +
                "userName='" + userName + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", userStatus=" + userStatus +
                ", name='" + name + '\'' +
                ", email='" + email + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}
