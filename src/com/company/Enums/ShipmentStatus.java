package com.company.Enums;

public enum ShipmentStatus {
    Pending,
    Shipped,
    Delivered,
    OnHold
}
