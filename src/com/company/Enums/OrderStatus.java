package com.company.Enums;

public enum OrderStatus {
    Unshipped,
    Pending,
    Shipped,
    Complete,
    Canceled,
    RefundApplied
}
