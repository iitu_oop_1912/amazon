package com.company.Enums;

public enum PaymentStatus {
    Unpaid ("Unpaid"),
    Pending ("Pending"),
    Completed ("Completed"),
    Failed ("Failed"),
    Declined ("Declined"),
    Canceled ("Canceled"),
    Abandoned ("Abandoned"),
    Settling ("Settling"),
    Settled ("Settled"),
    Refunded ("Refunded");

    private final String status;

    PaymentStatus(String status){
        this.status = status;
    }
    
    public String getStatus (){
        return this.status;
    }
}
