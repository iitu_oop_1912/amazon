package com.company.core;

import com.company.Product.Product;
import com.company.User.Admin;
import com.company.User.Member;
import com.company.categories.ProductCategory;

import java.util.ArrayList;

public class ShopManager{
    private static ArrayList<Member> allMembers;
    private static ArrayList<Admin> allAdmins;
    private static ArrayList<Product> allProducts;
    private static ArrayList<ProductCategory> allCategories;

    static {
        allMembers.add (new Member());
        allAdmins.add (new Admin ());
        allProducts.add ( new Product());
        allCategories.add ( new ProductCategory());
    }

    public static void addMember (Member member){
        allMembers.add (member);
    }

    public static void setAllProducts(ArrayList<Product> allProducts) {
        ShopManager.allProducts = allProducts ;
    }

    public static ArrayList<Product> getAllProducts () {
        return allProducts;
    }

    public static void setAllCategories(ArrayList<ProductCategory> allCategories) {
        ShopManager.allCategories = allCategories;
    }

    public static ArrayList<ProductCategory> getAllCategories () {
        return allCategories;
    }


	public static ArrayList<Admin> getAllAdmins() {
		return allAdmins;
    }
    
    static {
        allMembers = null;
        allAdmins = null;
        allProducts = null;
        allCategories = null;
    }

    public static void setAllAdmins(ArrayList<Admin> allAdmins) {
        ShopManager.allAdmins = allAdmins;
    }

    public static ArrayList<Member> getAllMembers() {
        return allMembers;
    }

    public static void setAllMembers(ArrayList<Member> allMembers) {
        ShopManager.allMembers = allMembers;
    }

	public static void Message() {
		System.out.println("Changed");
	}
    
    // public static ArrayList<Product> showProductsByCategory (String categoryName){
    //     ArrayList <Product> result = new ArrayList<>();
    //     for (Product i : allProduct) 
    //     {
    //         if (i.getCategory().getName() == categoryName) 
    //         {
	// 			result.add(i);
	// 		}
    //     }
    //     return result;
    // }
}
