package com.company.core;

import java.util.ArrayList;

import com.company.Product.Product;
import com.company.categories.ProductCategory;

public interface ISearch {
     public ArrayList<Product> SearchProductsById(int str);
     public ArrayList<Product> SearchProductsByCategory(String str);
     
     
     
}
