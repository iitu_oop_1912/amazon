package com.company.Product;

import java.util.ArrayList;
import java.util.Date;

import com.company.core.ISearch;
import com.company.core.ShopManager;

public class Catalog implements ISearch{

    private Date lastUpdatedDate = new Date();

//    private Map<String, list<Product>>  productNames = new Map<>();
//    private Map<String, list<Product>>  productCategories = new Map<>();
//    private String name;

public Catalog(){

}

public ArrayList <Product> SearchProductsByCategory(String str){
    ArrayList <Product> result = new ArrayList<>();
    for (Product i : ShopManager.getAllProducts()) 
    {
        if (i.getCategory().getName().toLowerCase() == str) 
        {
            result.add(i);
        }
    }
    return result;
}

public ArrayList<Product> SearchProductsById(int id){
    ArrayList <Product> result = new ArrayList<>();
    for (Product i : ShopManager.getAllProducts()) 
    {
        if (i.getId() == id) 
        {
            result.add(i);
        }
    }
    return result;
}

}
