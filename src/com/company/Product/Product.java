package com.company.Product;

import java.util.ArrayList;
import java.util.Scanner;

import com.company.categories.ProductCategory;

public class Product {
    private static int lastId;
    private int id;
    private String name;
    private String description;
    private double price;
    private int availableItemCount;
    private ProductCategory category;
    private ArrayList <ProductReview> reviews;

    static {
        lastId = 0;
    }

    public Product(String name, String description, double price, int availableItemCount, ProductCategory category) {
        this.id = lastId;
        lastId++;
        this.name = name;
        this.description = description;
        this.price = price;
        this.availableItemCount = availableItemCount;
        this.category = category;
    }
    public Product (){
        Scanner sc = new Scanner (System.in);
        System.out.println ("Add Product Name:");
        this.id = lastId;
        lastId++;
        String name = sc.nextLine();
        System.out.println ("Add description");
        String description = sc.nextLine ();
        System.out.println ("Add price");
        int price = sc.nextInt ();
        sc.next();
        System.out.println ("Amount of available Items");
        int available = sc.nextInt();
        //String category = sc.next();
        this.name = name;
        this.description = description;
        this.price = price;
        this.availableItemCount = available;
        this.category = null;
    }
    public void ModifyProduct (){
        Scanner sc = new Scanner (System.in);
        System.out.println ("Add Product Name:");
        String name = sc.nextLine();
        System.out.println ("Add description");
        String description = sc.nextLine ();
        System.out.println ("Add price");
        int price = sc.nextInt ();
        sc.next();
        System.out.println ("Amount of available Items");
        int available = sc.nextInt();
        //String category = sc.next();
        this.name = name;
        this.description = description;
        this.price = price;
        this.availableItemCount = available;
        this.category = null;
    }

    public ArrayList<ProductReview> getReviews() {
        return this.reviews;
    }

    public void setReviews(ArrayList<ProductReview> reviews) {
        this.reviews = reviews;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAvailableItemCount() {
        return availableItemCount;
    }

    public void setAvailableItemCount(int availableItemCount) {
        this.availableItemCount = availableItemCount;
    }

    public ProductCategory getCategory() {
        return category;
    }

    public void setCategory(ProductCategory category) {
        this.category = category;
    }

    public int getItemCount(){
        return availableItemCount;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", price=" + price +
                ", availableItemCount=" + availableItemCount +
                ", category=" + category +
                '}';
    }
}
