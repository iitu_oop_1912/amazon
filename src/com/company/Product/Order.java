package com.company.Product;

import com.company.Bank.CreditCardTransaction;
import com.company.Bank.ElectronicBankTransaction;
import com.company.Bank.Payment;
import com.company.System.Notification;
import com.company.System.Shipment;
import com.company.System.ShoppingCart;
import com.company.User.Member;
import com.company.core.ShopManager;
import com.company.Enums.OrderStatus;
import com.company.Enums.PaymentStatus;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Scanner;

public class Order {
    private Long OrderNumber;
    private static Long lastOrderNumber;
    private OrderStatus status;
    private Date orderDate = new Date();
    private Payment payment;
    private ArrayList<OrderLog> logs;
    private ShoppingCart itemSet;
    private Shipment shipment;
    private int ownerId;

    static {
        lastOrderNumber = 0L;
    }

    public Order(ShoppingCart itemSet,int accountId) {
        this.OrderNumber = lastOrderNumber;
        lastOrderNumber ++;
        this.status = OrderStatus.Unshipped;
        this.orderDate = new Date ();
        this.logs = new ArrayList<>();
        this.itemSet = itemSet;
        this.ownerId = accountId;
        if (MakePayment() == true){
            MakeShipment ();
        }
    }
    private boolean MakePayment(){
        Scanner sc = new Scanner(System.in);
        //payment = new Payment (PaymentStatus.Unpaid, itemSet.CalculatePrice());
        System.out.println("Please choose your payment method:");
        System.out.println("1)Electronic Bank Transfer");
        System.out.println("2)Credit Bank Transfer");
        int choose = sc.nextInt();
        sc.close ();
        if (choose == 1){
            payment = new ElectronicBankTransaction();
            System.out.println("Success!");
            payment.setStatus(payment.processPayment(ownerId));
            this.sendNotification(payment.getStatus().toString());
            return true;
        }
        else if (choose == 2){
            payment = new CreditCardTransaction();
            System.out.println("Success!");
            payment.setStatus(payment.processPayment(ownerId));
            this.sendNotification(payment.getStatus().toString());
            return true;
        }
        else {
            System.out.println("Wrong Command");
        }
        return false;
    }
    private void MakeShipment (){
        Date date = new Date ();
        Calendar c = Calendar.getInstance(); 
        c.setTime(date); 
        c.add(Calendar.DATE, 1);
        date = c.getTime();
        this.shipment = new Shipment ( date, "By Hand");
    }

    public Long getOrderNumber() {
        return OrderNumber;
    }

    public void setOrderNumber(Long orderNumber) {
        this.OrderNumber = orderNumber;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }
    public boolean sendForShipment(){
        return true;
    }
    @Override
    public String toString() {
        return "Order{" +
                "OrderNumber='" + OrderNumber + '\'' +
                ", status=" + status +
                ", orderDate=" + orderDate +
                '}';
    }
    public boolean sendNotification (String content){
        for (Member i : ShopManager.getAllMembers()){
            if (i.getAccount().getId() == this.ownerId){
                i.addNotification(new Notification(content));
                return true;
            }
        }
        return false;
    }
}
