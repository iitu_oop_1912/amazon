package com.company.Product;

import com.company.Product.Product;
import java.util.Scanner;

public class Item {
    private int quantity;
    private double price;
    private Product product;
 
    public Item(Product product) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Write amount");
        int quantity = sc.nextInt();
        double price = product.getPrice();
        this.quantity = quantity;
        this.price = price;
    }

    public Product getProduct() {
        return this.product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }
    

    public Item(){}


    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean UpdateQuantity(){
        return false;
    }

    @Override
    public String toString() {
        return "Item{" +
                "quantity=" + quantity +
                ", price=" + price +
                '}';
    }
}
