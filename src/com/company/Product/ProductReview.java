package com.company.Product;
import java.util.Scanner;

public class ProductReview {
    protected int rating;
    protected String review;

    public ProductReview(int rating, String review) {
        this.rating = rating;
        this.review = review;
    }

    public ProductReview(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Type something below in one Line...");
        String review = sc.nextLine();
        System.out.println("Rate this product from 0 to 10");
        int rating = sc.nextInt();
        sc.next();
        this.rating = rating;
        this.review = review;
    }

    public int getRating(){
        return rating;
    }
    
    @Override
    public String toString() {
        return "ProductReview{" +
                "rating=" + rating +
                ", review='" + review + '\'' +
                '}';
    }

}
