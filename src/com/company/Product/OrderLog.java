package com.company.Product;

import com.company.Enums.OrderStatus;

import java.util.Date;

public class OrderLog {

    private Date creationDate = new Date();
    private OrderStatus status;

    public OrderLog(Date creationDate, OrderStatus status) {
        this.creationDate = creationDate;
        this.status = status;
    }

    public OrderLog() {
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public OrderStatus getStatus() {
        return status;
    }

    public void setStatus(OrderStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "OrderLog{" +
                "creationDate=" + creationDate +
                ", status=" + status +
                '}';
    }
}
